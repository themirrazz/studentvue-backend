/*
See https://gitlab.com/themirrazz/studentvue-backend/-/blob/main/LICENSE.txt for licensing information
*/
const StudentVue=require('./studentvue')
const http=require('http');


const host=http.createServer(_handle_connection);
/**
 * @param req{http.IncomingMessage}
 * @param res{http.ServerResponse}
 */
function _handle_connection(req,res) {
    var url=new URL("https://your.mom"+req.url);
    if(req.method!="POST") {
        res.writeHead(400,{"content-type":'text/html','Access-control-allow-origin':'*'})
        res.write("<h1>pls use post and dond use facebooook bc it iz fakeboooook and dey r calld meta bc dey dond delete ur imgz metadataaaaaaaaa")
        res.end()
        return
    }
    var body="";
    req.on('data',function(chunk) {
        body+=chunk
    });
    req.on('end', function() {
        if(url.pathname==="/api/locate") {
            StudentVue.getDistrictUrls(body).then(function(raw) {
                var data=JSON.parse(raw)
                res.writeHead(200,{'Access-control-allow-origin':'*','Content-type':'text/plain'})
                res.write(JSON.stringify(data.DistrictLists.DistrictInfos.DistrictInfo))
                res.end()
            }).catch(function(e){
                res.writeHead(500,{"Content-type":'text/plain','Access-control-allow-origin':'*'});
                res.write("500 Internal Server Error ("+e.name+")")
                res.end()
            })
        } else if(url.pathname==="/api/messages") {
            var username=(new URLSearchParams(body)).get("username");
            var password=(new URLSearchParams(body)).get("password");
            var server=(new URLSearchParams(body)).get("server");
            StudentVue.login(server,username,password).then(function(client) {
                client.getMessages().then(function(messages) {
                    res.writeHead(200,{'Access-control-allow-origin':'*','Content-type':'text/plain'})
                    res.write(
                        JSON.stringify(
                            JSON.parse(messages).PXPMessagesData.MessageListings.MessageListing
                        )
                    );
                    res.end()
                })
            }).catch(function(e){
                res.writeHead(500,{"Content-type":'text/plain','Access-control-allow-origin':'*'});
                res.write("500 Internal Server Error ("+e.name+")")
                res.end()
            })
        } else if(url.pathname==="/api/calendar") {
            var username=(new URLSearchParams(body)).get("username");
            var password=(new URLSearchParams(body)).get("password");
            var server=(new URLSearchParams(body)).get("server");
            StudentVue.login(server,username,password).then(function(client) {
                client.getCalendar().then(function(messages) {
                    res.writeHead(200,{'Access-control-allow-origin':'*','Content-type':'text/plain'})
                    res.write(
                        JSON.stringify(
                            JSON.parse(messages).CalendarListing.EventLists.EventList
                        )
                    );
                    res.end()
                })
            }).catch(function(e){
                res.writeHead(500,{"Content-type":'text/plain','Access-control-allow-origin':'*'});
                res.write("500 Internal Server Error ("+e.name+")")
                res.end()
            })
        } else if(url.pathname==="/api/gradebook") {
            var username=(new URLSearchParams(body)).get("username");
            var password=(new URLSearchParams(body)).get("password");
            var server=(new URLSearchParams(body)).get("server");
            var params=new URLSearchParams(body)
            StudentVue.login(server,username,password).then(function(client) {
                client.getGradebook(params.get("period")).then(function(data) {
                    res.writeHead(200,{'Access-control-allow-origin':'*','Content-type':'text/plain'})
                    res.write(
                        JSON.stringify(
                            JSON.parse(data).Gradebook.ReportingPeriods.ReportingPeriod
                        )
                    );
                    res.end()
                })
            }).catch(function(e){
                res.writeHead(500,{"Content-type":'text/plain','Access-control-allow-origin':'*'});
                res.write("500 Internal Server Error ("+e.name+")")
                res.end()
            })
        } else if(url.pathname==="/api/attendance") {
            var username=(new URLSearchParams(body)).get("username");
            var password=(new URLSearchParams(body)).get("password");
            var server=(new URLSearchParams(body)).get("server");
            var params=new URLSearchParams(body)
            StudentVue.login(server,username,password).then(function(client) {
                client.getGradebook().then(function(data) {
                    res.writeHead(200,{'Access-control-allow-origin':'*','Content-type':'text/plain'})
                    res.write(
                        JSON.stringify(
                            JSON.parse(data).Attendance.Absences.Absence
                        )
                    );
                    res.end()
                })
            }).catch(function(e){
                res.writeHead(500,{"Content-type":'text/plain','Access-control-allow-origin':'*'});
                res.write("500 Internal Server Error ("+e.name+")")
                res.end()
            })
        } else if(url.pathname==="/api/student") {
            var username=(new URLSearchParams(body)).get("username");
            var password=(new URLSearchParams(body)).get("password");
            var server=(new URLSearchParams(body)).get("server");
            var params=new URLSearchParams(body)
            StudentVue.login(server,username,password).then(function(client) {
                client.getStudentInfo().then(function(data) {
                    res.writeHead(200,{'Access-control-allow-origin':'*','Content-type':'text/plain'})
                    res.write(
                        JSON.stringify(
                            JSON.parse(data).StudentInfo
                        )
                    );
                    res.end()
                })
            }).catch(function(e){
                res.writeHead(500,{"Content-type":'text/plain','Access-control-allow-origin':'*'});
                res.write("500 Internal Server Error ("+e.name+")")
                res.end()
            })
        } else if(url.pathname==="/api/schedule") {
            var username=(new URLSearchParams(body)).get("username");
            var password=(new URLSearchParams(body)).get("password");
            var server=(new URLSearchParams(body)).get("server");
            var params=new URLSearchParams(body)
            StudentVue.login(server,username,password).then(function(client) {
                client.getSchedule(params.get("term")).then(function(data) {
                    res.writeHead(200,{'Access-control-allow-origin':'*','Content-type':'text/plain'})
                    res.write(
                        JSON.stringify(
                            JSON.parse(data).StudentClassSchedule.ClassLists
                        )
                    );
                    res.end()
                })
            }).catch(function(e){
                res.writeHead(500,{"Content-type":'text/plain','Access-control-allow-origin':'*'});
                res.write("500 Internal Server Error ("+e.name+")")
                res.end()
            })
        } else {
            res.writeHead(404,{'content-type':'text/plain','access-control-allow-origin':'*'})
            res.write(`======= 404 REQUEST NOT FOUND =======
Your request was not found over here,
you may want to try looking somewhere
else for your request. Anywhere except
for Facebook, that is :)
======================================`);
            res.end()
        }
    })
}

host.listen(80);
